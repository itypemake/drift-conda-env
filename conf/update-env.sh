#!/usr/bin/env bash

cd ~/miniconda/3/conf || exit 1

echo -n "Adding content to the .bashrc file... "

cat bashrc-update.txt >> ~/.bashrc

echo " DONE"

echo ""
echo "Either source your .bashrc file to update your environment   "
echo "variables. Or logout and log back in. By running the command "
echo ""
echo " source ~/.bashrc"
echo ""
